#-------------------------------------------------
#
# Project created by QtCreator 2014-03-17T16:34:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZPO
TEMPLATE = app

CONFIG += resources


SOURCES += main.cpp\
        mainwindow.cpp \
    len.cpp

HEADERS  += mainwindow.h \
    len.h

FORMS    += mainwindow.ui

#RESOURCES += images.qrc

QMAKE_CXXFLAGS += -std=c++11
