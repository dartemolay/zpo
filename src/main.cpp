#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow window;
    window.showMaximized();
    window.show();
    return a.exec();
}


