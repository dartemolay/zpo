#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore>
#include <QFileDialog>
#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QProgressDialog>

#include "QImage"
#include "QMessageBox"
#include "QWidget"
#include "QDebug"
#include <math.h>
#include <cstdlib>

#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/random.hpp>
#include <glm/glm/gtx/intersect.hpp>
#include <glm/glm/gtx/vector_angle.hpp>
#include <glm/glm/gtx/rotate_vector.hpp>

#include "len.h"

class Len;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    glm::vec3 reflectRay(glm::vec3 orig, glm::vec3 ray, const Len &len, glm::vec3 &intersect);
    void lenClicked();

private slots:
    void on_fileSystemButton_clicked();
    void on_loadImageButton_clicked();
    void on_saveImageBtn_clicked();
    void on_pushButtonDistort_clicked();
    void on_doubleSpinBox_valueChanged(double arg1);
    void on_addConcaveLensBtn_clicked();
    void on_addConvexLensBtn_clicked();
    void itemSelectionChange();
    void on_deleteLenButton_clicked();
    void on_distanceLensSpinBox_valueChanged(double arg1);
    void on_refractiveIndexSpinBox_valueChanged(double arg1);
    void on_radiusSpinBox_valueChanged(double arg1);
    void on_thicknessSpinBox_valueChanged(double arg1);

private:
    Ui::MainWindow *ui;
    QString fileName;
    int counter = 0;
    QRect *rect1;
    QRect *rect2;
    QList<Len *> lenList;
    QGraphicsScene *scene;
    QGraphicsPixmapItem* item;
    QPixmap pixmap;
    QImage im2;
    QProgressDialog *progressDialog;
    double Zdistance;
};

#endif // MAINWINDOW_H
