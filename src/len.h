#ifndef LEN_H
#define LEN_H

#include <QtGui>
#include <QtCore>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include "mainwindow.h"

class MainWindow;

class Len : public QGraphicsPixmapItem
{

public:

    Len(bool c, double dis, double ind, double r1, double th, int counter, MainWindow *parent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    int    id;
    double distance;
    double radius;
    double index;
    double thickness;
    bool   convex;

    MainWindow *parent;
};

#endif // LEN_H
