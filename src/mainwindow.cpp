#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>

using namespace std;

/**
 * ==========================================================================================
 * Porovnavaci funkce pro serazeni cocek v seznamu. Seradi se podle vzdalenosti od
 * zdrojoveho obrazku
 */

bool myLess(const Len *a,const Len *b)
{
    return (a->distance) < (b->distance);
}

/**
 * ==========================================================================================
 * Inicializace okna aplikace, zobrazeni sceny.
 */

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene();
    ui->lensDisplay->setScene(scene);
    scene->setSceneRect(0,0,800,160);
    connect(scene, SIGNAL(selectionChanged()),this, SLOT(itemSelectionChange()));

    Zdistance=1000.0;

    progressDialog = new QProgressDialog("Processing image..", "Abort", 0, 100, this);
    progressDialog->setWindowModality(Qt::WindowModal);

    rect1 = new QRect(0,0, 20, 157);
    rect2 = new QRect(775,0, 20, 157);
    scene->addRect(*rect1);
    scene->addRect(*rect2);
}

/**
 * ==========================================================================================
 */

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * ==========================================================================================
 * Pro paprsek vyslany z pixelu vysledneho obrazku primo na cocku. Vraci vektor paprsku po
 * odrazeni na rozhrani cocky.
 */

glm::vec3 MainWindow::reflectRay(glm::vec3 orig, glm::vec3 ray, const Len &len, glm::vec3 &intersect)
{
    double N1=1.0;
    double N2=len.index;

    glm::vec3 p1(orig);
    glm::vec3 p2(orig+ray);


   // double size=sqrt(pow(len.radius,2) - pow(len.radius-len.thickness,2)); // polomer cocky
    glm::vec3 cent(0,0,len.distance + len.radius - len.thickness/2);
    float r=len.radius;



    //const double t= (len.distance- orig.z)/ray.z;
    //glm::vec3 pos(orig.x+ t*ray.x,orig.y+ t*ray.y,orig.z+ t*ray.z);
    //qDebug() << pos.x << pos.y << pos.z;
    //if (glm::distance(pos, glm::vec3(0,0,len.distance)) > size)
      //      return glm::vec3(0,0,0);



    if (len.convex)
    {


        glm::vec3 i1;
        glm::vec3 i2;
        glm::vec3 n1;
        glm::vec3 n2;
        glm::intersectLineSphere(p1, p2, cent, r, i1, n1, i2, n2);

        if (n1.x ==0 && n1.y == 0 && n1.z == 0 && n2.x ==0 && n2.y == 0 && n2.z == 0 )
            return n1;

        // prusecik s nizsi zetovou souradnici
        if (i2.z < i1.z)
        {
            i1=i2;
            n1=n2;
        }

        // paprsek i normala jsou stejny
        if (glm::normalize(ray)== n1 )
            return ray;

        double oldAngle = glm::angle(glm::normalize(ray), n1);
        double newAngle=asin((N1/N2)*sin( oldAngle *M_PI/180));
        newAngle=newAngle*180/M_PI;

        glm::vec3 newRay;
        glm::vec3 norm =glm::cross(ray, n1);

        if (oldAngle-newAngle != 0)
            newRay=glm::rotate(ray, (glm::mediump_float)(oldAngle-newAngle), norm);
        else
            newRay=ray;


        //qDebug()<<  size << glm::distance(i1, glm::vec3(0,0,len.distance));





/*        cent= glm::vec3(0,0,len.distance + len.thickness/2 - len.radius);

        p1 = glm::vec3(i1);
        p2 = glm::vec3(i1 + newRay);
        glm::intersectLineSphere(p1, p2, cent, r, i1, n1, i2, n2);

        if (n1.x ==0 && n1.y == 0 && n1.z == 0 && n2.x ==0 && n2.y == 0 && n2.z == 0 )
            return n1;

        if (i2.z > i1.z)
        {
            i1=i2;
            n1=n2;
        }
        if (glm::normalize(newRay)== n1 )
            return newRay;

        oldAngle = glm::angle(glm::normalize(newRay), n1);
        newAngle=asin((N2/N1)*sin( oldAngle *M_PI/180));
        newAngle=newAngle*180/M_PI;

        norm =glm::cross(newRay, -n1);

        newRay=glm::rotate(newRay, (glm::mediump_float)(oldAngle-newAngle), norm);
*/

        intersect=i1;
        return newRay;
    }
    else
    {
        glm::vec3 cent(0,0,len.distance - len.radius - len.thickness/2);
        float r=len.radius;

        glm::vec3 i1;
        glm::vec3 i2;
        glm::vec3 n1;
        glm::vec3 n2;
        glm::intersectLineSphere(p1, p2, cent, r, i1, n1, i2, n2);

        if (n1.x ==0 && n1.y == 0 && n1.z == 0 && n2.x ==0 && n2.y == 0 && n2.z == 0 )
            return n1;

        // prusecik s nizsi zetovou souradnici
        if (i2.z > i1.z)
        {
            i1=i2;
            n1=n2;
        }

        // paprsek i normala jsou stejny
        if (glm::normalize(ray)== n1 )
            return ray;

        double oldAngle = glm::angle(glm::normalize(ray), n1);
        double newAngle=asin((N1/N2)*sin( oldAngle *M_PI/180));
        newAngle=newAngle*180/M_PI;

        glm::vec3 newRay;
        glm::vec3 norm =glm::cross(ray, n1);

        if (oldAngle-newAngle != 0)
            newRay=glm::rotate(ray, (glm::mediump_float)(oldAngle-newAngle), norm);
        else
            newRay=ray;

        /*
        cent= glm::vec3(0,0,len.distance + len.thickness/2 - len.radius);
        p1 = i1;
        p2 = i1+newRay;
        glm::intersectLineSphere(p1, p2, cent, r, i1, n1, i2, n2);

        if (i2.z > i1.z)
        {
            i1=i2;
            n1=n2;
        }
        if (glm::normalize(newRay)== n1 )
            return newRay;

        oldAngle = glm::angle(glm::normalize(newRay), n1);
        newAngle=asin((N2/N1)*sin( oldAngle *M_PI/180));
        newAngle=newAngle*180/M_PI;

        norm =glm::cross(newRay, n1);

        newRay=glm::rotate(newRay, (glm::mediump_float)(oldAngle-newAngle), norm);

        */

        intersect=i1;
        return newRay;
    }

}

/**
 * ==========================================================================================
 * Kliknuti na tlacitko "File System".
 * Zobrazeni souboroveho systemu a ulozeni cesty ke zdrojovemu obrazku do promenne
 */

void MainWindow::on_fileSystemButton_clicked()
{
    fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.jpg *.png *.bmp)"));
    ui->lineEdit->setText(fileName);
}

/**
 * ==========================================================================================
 * Kliknuti na tlacitlo "Load Image".
 * Nacteni obrazku, uprava jeho velikosti, zobrazeni.
 */

void MainWindow::on_loadImageButton_clicked()
{
    if (fileName.isEmpty())
    {
        QImage image(fileName);
        if (image.isNull())
            QMessageBox::information(this, tr("Error"), tr("Cannot load %1").arg(fileName));
        return;
    }

    pixmap.load(fileName);

    if (pixmap.width() > pixmap.height())
    {
        if (pixmap.width() < 1000)
            ui->imageLabel->setPixmap(pixmap);
        else
            ui->imageLabel->setPixmap(pixmap.scaled(1000,pixmap.height()/(pixmap.width()/1000),Qt::KeepAspectRatio));
    }

    if (pixmap.height() < 500)
        ui->imageLabel->setPixmap(pixmap);
    else
        ui->imageLabel->setPixmap(pixmap.scaled(pixmap.width()/(pixmap.height()/500), 500,Qt::KeepAspectRatio));

    ui->imageLabel->setAlignment(Qt::AlignHCenter);
    ui->imageLabel->show();
}

/**
 * ==========================================================================================
 * Kliknuti na tlacitko "Save Image".
 * Ulozeni cockami zdeformovaneho obrazku
 */

void MainWindow::on_saveImageBtn_clicked()
{
    if ((im2.width() == 0) && (im2.height() == 0))
    {
        QMessageBox::information(this, tr("Error"), tr("Cannot save. %1").arg(fileName));
        return;
    }

    QString fileName2 = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Images (*.jpg *.png *.bmp)"));

    if (!im2.save(fileName2))
    {
        QMessageBox::information(this, tr("Error"), tr("Saving failed. %1").arg(fileName));
        return;
    }
}

/**
 * ==========================================================================================
 * Kliknuti na tlacitko "Distort".
 *
 */

void MainWindow::on_pushButtonDistort_clicked()
{
    if ( lenList.size() == 0)
        return;

    qSort(lenList.begin(), lenList.end(),myLess);

    if (pixmap.isNull())
    {
        QMessageBox::information(this, tr("Error"), tr("No image. %1").arg(fileName));
        return;
    }

    QImage im;
    im=ui->imageLabel->pixmap()->toImage();

    const int imh = im.height();
    const int imw = im.width();

    im2 = im.copy(0,0,imw,imh);
    im2.fill(Qt::black);
    bool nextRay= false;

    // pres Y osu obrazku
    for (int i=0; i < imw;i++)
    {
        // pres X osu obrazku
        for (int j=0; j < imh;j++)
        {

            glm::vec3 ray(i-imw/2, j-imh/2, Zdistance); // prvni paprsek
            glm::vec3 orig(i-imw/2, j-imh/2, 0); // vychozi bod prvniho paprsku
            glm::vec3 intersect, pos;
            glm::vec3 newRay;

            nextRay=false;

            // vypocet paprsku pro vsechny cocky

            for (int l=0; l < lenList.size(); l++)
            {
                Len *len=lenList.at(l);

                newRay= reflectRay(orig, ray, *len, intersect);

                if (newRay.x == 0 && newRay.y == 0 && newRay.z == 0 || newRay.x != newRay.x)
                {
                    nextRay=true;
                    break;
                }
                else
                    nextRay=false;

                orig=intersect;
                ray=newRay;

            }

            if (!nextRay)
            {

                // prusecik se zdrojovym obrazem
                const double t= (Zdistance- intersect.z)/newRay.z;
                pos=glm::vec3(intersect.x+ t*newRay.x,intersect.y+ t*newRay.y,intersect.z+ t*newRay.z);

                const double posx=pos.x+((double)imw*0.5);
                const double posy=pos.y+((double)imh*0.5);

                //qDebug() << "pos:" << pos.x << pos.y << pos.z << t ;

                // kdyz padne paprsek mimo obraz, tak se ignoruje
                if (posx < 0.0 || posx >= imw-1 || posy < 0.0 || posy >= imh-1)
                    continue;

                // vzdalenosti k okolnim pixelum
                double posx_vlevo = floor(posx);
                double posx_vpravo = ceil(posx);
                double posy_dole = floor(posy);
                double posy_nahore = ceil(posy);

                if (posx_vlevo == posx_vpravo) posx_vpravo += 1.0;
                if (posy_nahore == posy_dole) posy_nahore += 1.0;

                // vazeny prumer z 2 hornich okolnich pixelu
                QColor vlevo_nahore = QColor(im.pixel(posx_vlevo , posy_nahore));
                glm::vec3 x_vlevo_nahore(vlevo_nahore.red(),vlevo_nahore.green(),vlevo_nahore.blue());
                x_vlevo_nahore *= fabs(posx - posx_vpravo);

                QColor vpravo_nahore = QColor(im.pixel(posx_vpravo , posy_nahore));
                glm::vec3 x_vpravo_nahore(vpravo_nahore.red(),vpravo_nahore.green(),vpravo_nahore.blue());
                x_vpravo_nahore *= fabs(posx - posx_vlevo);

                glm::vec3 x_nahore(x_vlevo_nahore+x_vpravo_nahore);
                x_nahore *= fabs(posy_dole-posy);


                // vazeny prumer z 2 dolnich okolnich pixelu
                QColor vlevo_dole = QColor(im.pixel(posx_vlevo , posy_dole));
                glm::vec3 x_vlevo_dole(vlevo_dole.red(),vlevo_dole.green(),vlevo_dole.blue());
                x_vlevo_dole *= fabs(posx - posx_vpravo);

                QColor vpravo_dole = QColor(im.pixel(posx_vpravo , posy_dole));
                glm::vec3 x_vpravo_dole(vpravo_dole.red(),vpravo_dole.green(),vpravo_dole.blue());
                x_vpravo_dole *= fabs(posx - posx_vlevo);

                glm::vec3 x_dole(x_vlevo_dole + x_vpravo_dole);
                x_dole *= fabs(posy_nahore-posy);


                // celkove vyinterpolovane
                glm::vec3 celkem = x_nahore + x_dole;

                //
                QColor out (celkem.x,celkem.y,celkem.z);
                im2.setPixel(i, j,out.rgb() );

            }

        }

    }

    ui->imageLabel->setPixmap(QPixmap::fromImage(im2));
}

/**
 * ==========================================================================================
 * Pri zmene pole "Distance between images [px]".
 * Dojde k nastaveni nove vzdalenosti mezi obrazky a k prepocitani hodnot distance u objektu
 * cocek.
 */

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    for (int i=0; i < lenList.size(); ++i) {
        lenList.at(i)->distance *= arg1/Zdistance;
        ui->distanceLensSpinBox->setValue(lenList.at(i)->distance);
    }
    Zdistance=arg1;
}

/**
 * ==========================================================================================
 * Dojde-li k vyberu cocky ve scene opticke soustavy, predvyplni se napravo policka parametru
 * cocky.
 */

void MainWindow::itemSelectionChange()
{
    QList<QGraphicsItem *> list= scene->selectedItems();

    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());
        ui->distanceLensSpinBox->setValue(len->distance);
        ui->refractiveIndexSpinBox->setValue(len->index);
        ui->radiusSpinBox->setValue(len->radius);
        ui->thicknessSpinBox->setValue(len->thickness);
    }
}

/**
 * ==========================================================================================
 * Posunuti cocky ve scene opticke soustavy uzivatelem aplikace. Aktualizuje se pozice cocky.
 */

void MainWindow::lenClicked()
{
    QList<QGraphicsItem *> list= scene->selectedItems();

    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());
        int pos= len->pos().x();

        if (pos < 25)
            pos=25;
        if (pos > 735)
            pos = 735;

        len->setPos(pos, 2);

        ui->distanceLensSpinBox->setValue( ( ((double)pos/800.0)*(double)Zdistance) );
        len->distance= ((double)pos/800.0)*(double)Zdistance;
    }
}

/**
 * ==========================================================================================
 * Kliknuti na tlacitko "Add convex len".
 * Dojde k vytvoreni noveho objektu spojne cocky a zobrazeni do opticke soustavy.
 */

void MainWindow::on_addConvexLensBtn_clicked()
{
    double distance = ui->distanceLensSpinBox->value();
    double index = ui->refractiveIndexSpinBox->value();
    double radius = ui->radiusSpinBox->value();
    double thickness = ui->thicknessSpinBox->value();

    Len* len = new Len(true, distance, index, radius, thickness, counter, this);
    counter++;

    lenList.append(len);
    len->setPos((int)(((double)distance/(double)Zdistance) * 800.0) , 2);
    scene->addItem(len);
}

/**
 * ==========================================================================================
 * Kliknuti na tlacitko "Add concave len".
 * Dojde k vytvoreni noveho objektu rozptylne cocky a zobrazeni cocky do opticke soustavy.
 */

void MainWindow::on_addConcaveLensBtn_clicked()
{
    double distance = ui->distanceLensSpinBox->value();
    double index = ui->refractiveIndexSpinBox->value();
    double radius = ui->radiusSpinBox->value();
    double thickness = ui->thicknessSpinBox->value();

    Len* len = new Len(false, distance, index, radius, thickness, counter, this);
    counter++;

    lenList.append(len);
    len->setPos((int)(((double)distance/(double)Zdistance) * 800.0) , 2);
    scene->addItem(len);
}

/**
 * ==========================================================================================
 * Kliknuti na tlacitko "Delete len".
 * Dojde k vymazani cocky vybrane v zobrazeni opticke soustavy.
 */

void MainWindow::on_deleteLenButton_clicked()
{
    QList<QGraphicsItem *> list= scene->selectedItems();

    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());

        for (int i=0; i<lenList.size(); i++)
        {
            if (lenList[i]->id == len->id)
                lenList.removeAt(i);
        }

        delete len;
    }
}

/**
 * ==========================================================================================
 * Zmena hodnoty vzdalenosti cocky od zdrojoveho obrazku.
 */

void MainWindow::on_distanceLensSpinBox_valueChanged(double arg1)
{
    QList<QGraphicsItem *> list= scene->selectedItems();
    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());
        len->distance=arg1;

        int pos=(int) ((arg1/Zdistance)* 800.0 );

        if (pos < 25)
            pos = 25;
        if (pos > 735)
            pos = 735;

        len->setPos(pos, 2);
    }
}

/**
 * ==========================================================================================
 * Zmena hodnoty indexu lomu cocky.
 */

void MainWindow::on_refractiveIndexSpinBox_valueChanged(double arg1)
{
    QList<QGraphicsItem *> list= scene->selectedItems();
    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());
        len->index=arg1;
    }
}

/**
 * ==========================================================================================
 * Zmena hodnoty polomeru zakriveni cocky.
 */

void MainWindow::on_radiusSpinBox_valueChanged(double arg1)
{
    QList<QGraphicsItem *> list= scene->selectedItems();
    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());
        len->radius=arg1;
    }
}

/**
 * ==========================================================================================
 * Zmena hodnoty tloustky cocky.
 */

void MainWindow::on_thicknessSpinBox_valueChanged(double arg1)
{
    QList<QGraphicsItem *> list= scene->selectedItems();
    if (list.count() == 1)
    {
        Len* len=qgraphicsitem_cast<Len *>(list.first());
        len->thickness=arg1;
    }
}
