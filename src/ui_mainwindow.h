/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *imageLabel;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *lineEdit;
    QPushButton *fileSystemButton;
    QPushButton *loadImageButton;
    QPushButton *pushButtonDistort;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBox;
    QPushButton *saveImageBtn;
    QSpacerItem *verticalSpacer;
    QFormLayout *formLayout;
    QHBoxLayout *horizontalLayout_2;
    QGraphicsView *lensDisplay;
    QVBoxLayout *leftLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *distanceLabel;
    QDoubleSpinBox *distanceLensSpinBox;
    QHBoxLayout *horizontalLayout_5;
    QLabel *refractiveIndexLabel;
    QDoubleSpinBox *refractiveIndexSpinBox;
    QHBoxLayout *horizontalLayout_7;
    QLabel *radiusLabel;
    QDoubleSpinBox *radiusSpinBox;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_2;
    QDoubleSpinBox *thicknessSpinBox;
    QVBoxLayout *lensVerticaLayout;
    QHBoxLayout *lensPanelLayout;
    QVBoxLayout *rightLayout;
    QPushButton *addConcaveLensBtn;
    QPushButton *addConvexLensBtn;
    QPushButton *deleteLenButton;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1244, 368);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        imageLabel = new QLabel(centralWidget);
        imageLabel->setObjectName(QStringLiteral("imageLabel"));
        imageLabel->setMinimumSize(QSize(1000, 0));
        imageLabel->setMaximumSize(QSize(1000, 500));
        imageLabel->setLineWidth(1000);

        horizontalLayout->addWidget(imageLabel);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetMinimumSize);
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_4->addWidget(lineEdit);

        fileSystemButton = new QPushButton(centralWidget);
        fileSystemButton->setObjectName(QStringLiteral("fileSystemButton"));

        horizontalLayout_4->addWidget(fileSystemButton);


        verticalLayout->addLayout(horizontalLayout_4);

        loadImageButton = new QPushButton(centralWidget);
        loadImageButton->setObjectName(QStringLiteral("loadImageButton"));

        verticalLayout->addWidget(loadImageButton);

        pushButtonDistort = new QPushButton(centralWidget);
        pushButtonDistort->setObjectName(QStringLiteral("pushButtonDistort"));

        verticalLayout->addWidget(pushButtonDistort);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMaximum(10000);
        doubleSpinBox->setSingleStep(1);
        doubleSpinBox->setValue(1000);

        verticalLayout->addWidget(doubleSpinBox);

        saveImageBtn = new QPushButton(centralWidget);
        saveImageBtn->setObjectName(QStringLiteral("saveImageBtn"));

        verticalLayout->addWidget(saveImageBtn);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));

        horizontalLayout->addLayout(formLayout);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lensDisplay = new QGraphicsView(centralWidget);
        lensDisplay->setObjectName(QStringLiteral("lensDisplay"));
        lensDisplay->setMaximumSize(QSize(800, 160));
        lensDisplay->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        lensDisplay->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        horizontalLayout_2->addWidget(lensDisplay);

        leftLayout = new QVBoxLayout();
        leftLayout->setSpacing(6);
        leftLayout->setObjectName(QStringLiteral("leftLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        distanceLabel = new QLabel(centralWidget);
        distanceLabel->setObjectName(QStringLiteral("distanceLabel"));

        horizontalLayout_3->addWidget(distanceLabel);

        distanceLensSpinBox = new QDoubleSpinBox(centralWidget);
        distanceLensSpinBox->setObjectName(QStringLiteral("distanceLensSpinBox"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(distanceLensSpinBox->sizePolicy().hasHeightForWidth());
        distanceLensSpinBox->setSizePolicy(sizePolicy);
        distanceLensSpinBox->setMinimumSize(QSize(100, 0));
        distanceLensSpinBox->setMaximumSize(QSize(100, 16777215));
        distanceLensSpinBox->setDecimals(0);
        distanceLensSpinBox->setMinimum(25);
        distanceLensSpinBox->setMaximum(10000);
        distanceLensSpinBox->setSingleStep(1);
        distanceLensSpinBox->setValue(150);

        horizontalLayout_3->addWidget(distanceLensSpinBox);


        leftLayout->addLayout(horizontalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        refractiveIndexLabel = new QLabel(centralWidget);
        refractiveIndexLabel->setObjectName(QStringLiteral("refractiveIndexLabel"));

        horizontalLayout_5->addWidget(refractiveIndexLabel);

        refractiveIndexSpinBox = new QDoubleSpinBox(centralWidget);
        refractiveIndexSpinBox->setObjectName(QStringLiteral("refractiveIndexSpinBox"));
        sizePolicy.setHeightForWidth(refractiveIndexSpinBox->sizePolicy().hasHeightForWidth());
        refractiveIndexSpinBox->setSizePolicy(sizePolicy);
        refractiveIndexSpinBox->setMinimumSize(QSize(100, 0));
        refractiveIndexSpinBox->setMaximumSize(QSize(100, 16777215));
        refractiveIndexSpinBox->setMinimum(1);
        refractiveIndexSpinBox->setMaximum(5);
        refractiveIndexSpinBox->setSingleStep(0.05);
        refractiveIndexSpinBox->setValue(1.5);

        horizontalLayout_5->addWidget(refractiveIndexSpinBox);


        leftLayout->addLayout(horizontalLayout_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        radiusLabel = new QLabel(centralWidget);
        radiusLabel->setObjectName(QStringLiteral("radiusLabel"));

        horizontalLayout_7->addWidget(radiusLabel);

        radiusSpinBox = new QDoubleSpinBox(centralWidget);
        radiusSpinBox->setObjectName(QStringLiteral("radiusSpinBox"));
        sizePolicy.setHeightForWidth(radiusSpinBox->sizePolicy().hasHeightForWidth());
        radiusSpinBox->setSizePolicy(sizePolicy);
        radiusSpinBox->setMinimumSize(QSize(100, 0));
        radiusSpinBox->setMaximumSize(QSize(100, 16777215));
        radiusSpinBox->setMinimum(0);
        radiusSpinBox->setMaximum(10000);
        radiusSpinBox->setSingleStep(0.5);
        radiusSpinBox->setValue(100);

        horizontalLayout_7->addWidget(radiusSpinBox);


        leftLayout->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_6->addWidget(label_2);

        thicknessSpinBox = new QDoubleSpinBox(centralWidget);
        thicknessSpinBox->setObjectName(QStringLiteral("thicknessSpinBox"));
        sizePolicy.setHeightForWidth(thicknessSpinBox->sizePolicy().hasHeightForWidth());
        thicknessSpinBox->setSizePolicy(sizePolicy);
        thicknessSpinBox->setMinimumSize(QSize(100, 0));
        thicknessSpinBox->setMaximumSize(QSize(100, 16777215));
        thicknessSpinBox->setMaximum(1000);
        thicknessSpinBox->setSingleStep(0.5);
        thicknessSpinBox->setValue(50);

        horizontalLayout_6->addWidget(thicknessSpinBox);


        leftLayout->addLayout(horizontalLayout_6);


        horizontalLayout_2->addLayout(leftLayout);

        lensVerticaLayout = new QVBoxLayout();
        lensVerticaLayout->setSpacing(7);
        lensVerticaLayout->setObjectName(QStringLiteral("lensVerticaLayout"));
        lensPanelLayout = new QHBoxLayout();
        lensPanelLayout->setSpacing(6);
        lensPanelLayout->setObjectName(QStringLiteral("lensPanelLayout"));
        rightLayout = new QVBoxLayout();
        rightLayout->setSpacing(6);
        rightLayout->setObjectName(QStringLiteral("rightLayout"));
        addConcaveLensBtn = new QPushButton(centralWidget);
        addConcaveLensBtn->setObjectName(QStringLiteral("addConcaveLensBtn"));

        rightLayout->addWidget(addConcaveLensBtn);

        addConvexLensBtn = new QPushButton(centralWidget);
        addConvexLensBtn->setObjectName(QStringLiteral("addConvexLensBtn"));

        rightLayout->addWidget(addConvexLensBtn);

        deleteLenButton = new QPushButton(centralWidget);
        deleteLenButton->setObjectName(QStringLiteral("deleteLenButton"));

        rightLayout->addWidget(deleteLenButton);


        lensPanelLayout->addLayout(rightLayout);


        lensVerticaLayout->addLayout(lensPanelLayout);


        horizontalLayout_2->addLayout(lensVerticaLayout);


        verticalLayout_2->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        imageLabel->setText(QString());
        fileSystemButton->setText(QApplication::translate("MainWindow", "File System", 0));
        loadImageButton->setText(QApplication::translate("MainWindow", "Load Image", 0));
        pushButtonDistort->setText(QApplication::translate("MainWindow", "Distort", 0));
        label->setText(QApplication::translate("MainWindow", "Distance between images [px]:", 0));
        saveImageBtn->setText(QApplication::translate("MainWindow", "Save Image", 0));
        distanceLabel->setText(QApplication::translate("MainWindow", "Distance from image [px]:", 0));
        refractiveIndexLabel->setText(QApplication::translate("MainWindow", "Refractive index:", 0));
        radiusLabel->setText(QApplication::translate("MainWindow", "Radius of curvature [px]:", 0));
        label_2->setText(QApplication::translate("MainWindow", "Len thickness [px]:", 0));
        addConcaveLensBtn->setText(QApplication::translate("MainWindow", "Add concave len", 0));
        addConvexLensBtn->setText(QApplication::translate("MainWindow", "Add convex len", 0));
        deleteLenButton->setText(QApplication::translate("MainWindow", "Delete selected len", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
