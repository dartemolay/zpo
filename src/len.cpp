#include "len.h"

Len::Len(bool c, double dis, double ind, double r1, double th, int counter, MainWindow *parent) : parent(parent)
{
    this->id = counter;
    this->convex = c;
    this->distance = dis;
    this->index = ind;
    this->radius = r1;
    this->thickness = th;

    QString path;
    if (c)
        path = "../src/spojka.bmp";
    else
        path = "../src/rozptylka.bmp";

    QImage lenImage(path);

    this->setPixmap(QPixmap::fromImage(lenImage));
    this->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
}

void Len::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    parent->lenClicked();
    QGraphicsPixmapItem::mouseReleaseEvent(event);
}


// AZ TO BUDE FUNGOVAT PRO JEDNU ČOČKU A BUDEME VKLÁDAT DALŠÍ ČOČKY
// VYTVOŘÍME VEKTOR, KTERÝ BUDE UCHOVÁVAT PŘESNÉ POŘADÍ ZA SEBOU JDOUCÍCH ČOČEKNA ZÁKLADĚ TOHO,
// NEBUDE TĚŽKÉ SPOČÍTAT VZDÁLENOST K DALŠÍMU LOMU
