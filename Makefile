# ZPO - deformace obrazu cockou
# xlatta00 xhrade08 xcerna01
#


.PHONY= all zpo clean run

all: zpo

zpo:
	qmake -makefile -o src/Makefile src/ZPO.pro
	make -C src

run:	
	./src/ZPO
	
clean:
	rm -rf src/*.o src/ZPO src/Makefile src/moc_*.c* src/qrc_images.cpp
    
